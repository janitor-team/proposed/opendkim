opendkim (2.11.0~beta2-4) UNRELEASED; urgency=medium

  * Update debhelper-compat to compatibility level 13.
    - Rename d/opendkim.tmpfile for new dh_installtmpfiles convention.
  * opendkim-tools: Reference "OpenDKIM" in synopsis.
  * miltertest: Drop patch moving the man page (rejected upstream).
  * Add missing DEP-3 headers tracking upstream bug in d/patches.

 -- David Bürgin <dbuergin@gluet.ch>  Fri, 24 Jul 2020 17:33:36 +0200

opendkim (2.11.0~beta2-3) unstable; urgency=medium

  * Move miltertest into a separate binary package (Closes: #947324).
  * miltertest: Add a patch that moves the man page to section 1.
  * Use dh_auto_configure instead of ./configure directly, in order to install
    libraries and pkg-config files in the multiarch path /usr/lib/*/.
  * Mark shared library packages as "Multi-Arch: same" in d/control.
  * Silence a debhelper warning by renaming d/opendkim.service.generate.
  * Bump Standards-Version to 4.5.0 without further changes.

 -- David Bürgin <dbuergin@gluet.ch>  Thu, 21 May 2020 15:06:20 +0200

opendkim (2.11.0~beta2-2) unstable; urgency=medium

  * Bump debhelper compat to compatibility level 12, necessitating many small
    changes in .install files and similar to silence dh_missing warnings.
  * General packaging overhaul:
    - Package synopses have been streamlined.
    - opendkim-tools: System utilities have moved from /usr/bin to /usr/sbin.
    - opendkim-tools: A few secondary statistics utilities are now included.
    - Various additional documentation is included.
    - Recommends: Promote opendkim-tools to opendkim recommends, because it is
      indispensable for initial setup and almost always installed together
      with opendkim.
    - Suggests: Drop unbound from opendkim suggests, because opendkim already
      uses libunbound, suggesting the standalone service may be misleading.
    - Remnants of obsolete (unused) "reputation" packages have been purged.
  * Additional patches:
    - opendkim-genzone: Fix inverted logic with subdomains (after a patch
      originally by Andreas Schulze).
    - opendkim: Suppress an empty pair of brackets logged to syslog.
  * Drop redundant specification of default config ("-x /etc/opendkim.conf")
    in systemd and init services.
  * Rewrite /etc/opendkim.conf and discourage use of /etc/default/opendkim
    (Closes: #861169).
  * postinst: Make script more robust when expected directories are not
    present (LP: #1787307).

 -- David Bürgin <dbuergin@gluet.ch>  Tue, 28 Jan 2020 10:13:12 +0100

opendkim (2.11.0~beta2-1) unstable; urgency=medium

  * New upstream release.
    - Refresh patches, delete patch "openssl_1.1.0_compat.patch" incorporated
      upstream
    - Update symbols files, add Build-Depends-Package metadata
    - Drop outdated --enable-poll and --with-test-socket configure options
  * debian/rules: Build with hardening flags (patch by Christian Göttsche
    <cgzones@googlemail.com>; closes: #878058)
  * opendkim.service.generate script: Fix permissions of generated override
    files (patch by Christophe Siraut <tobald@debian.org>; closes: #859797)
  * miltertest: Add patch fixing undefined behaviour in mt.eom_check()
    (Closes: #946397)
  * libopendkim-dev: Install only HTML files as documentation, and do so
    directly in /usr/share/doc/<package> (no subdirectory) as does upstream
  * Install convert_keylist.sh script in its documented location at
    /usr/bin/opendkim-convert-keylist, but keep old name around as a symlink
  * Replace all uses of /var/run with /run (Closes: #859706)
  * Add VCS repository coordinates
  * Bump standards version to 4.4.1 without further change

 -- David Bürgin <dbuergin@gluet.ch>  Tue, 31 Dec 2019 15:16:22 +0100

opendkim (2.11.0~alpha-14) unstable; urgency=medium

  * Bump debhelper compat to compatibility level 10
  * Bump Standards-Version to 4.3.0
  * debian/watch: Use uscan version 4, tighten URL mangling patterns; also
    remove unused and weak upstream signing key (upstream no longer signs
    release tarballs)
  * debian/copyright: Convert copyright information to DEP 5 format
  * debian/control: Update obsolete Priority "extra" to "optional"
  * Strip trailing whitespace in debian/*

 -- David Bürgin <dbuergin@gluet.ch>  Sun, 22 Dec 2019 11:31:08 +0100

opendkim (2.11.0~alpha-13) unstable; urgency=medium

  * miltertest: Fix broken mt.data() function (Closes: #946386)
  * Set maintainer to David Bürgin, who intends to adopt the package
    (Closes: #900774)

 -- David Bürgin <dbuergin@gluet.ch>  Sat, 14 Dec 2019 17:00:36 +0100

opendkim (2.11.0~alpha-12) unstable; urgency=medium

  * Orphan the package, see #900774, set maintainer to Debian QA Group
  * Drop Mark Markley from uploaders, not active Debian in years
  * Partial update of debian/watch to point to the new upstream repository

 -- Scott Kitterman <scott@kitterman.com>  Tue, 11 Dec 2018 19:49:47 -0500

opendkim (2.11.0~alpha-11) unstable; urgency=medium

  * Update opendkim service file so that /etc/opendkim.conf is used (Closes:
    #864162)
  * Start as root and drop privileges in opendkim so proper key file
    ownership works correctly
  * Add new options to /etc/opendkim.conf to match the above service file
    changes
  * Bump standards version to 4.1.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 03 Sep 2017 20:22:45 -0400

opendkim (2.11.0~alpha-10) unstable; urgency=medium

  * Do not remove /etc/default/opendkim on upgrade since it is a conffile
    because policy 10.7.3 (Closes: #863055)

 -- Scott Kitterman <scott@kitterman.com>  Mon, 22 May 2017 18:08:41 -0400

opendkim (2.11.0~alpha-9) unstable; urgency=medium

  * Set umask to 0007 in opendkim.service so opendkim socket is group readable
    (Closes: #851141)
  * Set opendkim-genkey to default to 2048 bit sha-256 only keys based on
    current best practices
  * Add debian/patches/nsupdate_output.patch to fix opendkim-genzone output to
    be usable for nsupdate (Closes: #849540)

 -- Scott Kitterman <scott@kitterman.com>  Wed, 25 Jan 2017 09:58:07 -0500

opendkim (2.11.0~alpha-8) unstable; urgency=medium

  [ Peter Colberg ]
  * On systems using systemd, this version replaces /etc/default/opendkim
    with the files /etc/systemd/system/opendkim.service.d/overrride.conf
    and /etc/tmpfiles.d/opendkim.conf carrying over non-default settings
    (Closes: #843463, #843494)

  [ Scott Kitterman ]
  * Fix undefined behavior in opendkim.postinst (Closes: #842684)
  * Fix pathfind invocation in sysv init script (Closes: #809089)

 -- Scott Kitterman <scott@kitterman.com>  Mon, 07 Nov 2016 09:07:28 -0500

opendkim (2.11.0~alpha-7) unstable; urgency=medium

  * Upload to unstable (Closes: #840015)
    - Despite being called an alpha it is almost entirely a bug fix release
  * Replace TimeoutStartSec=10 with Restart=on-failure in
    opendkim.service to give more time for initial start-up and to make
    sure opendkim starts eventually (Closes: #837376)
  * Fix group permissions on /var/run/opendkim (Closes: #837375)
  * Add additional information to README.Debian about configuration with
    Postfix (Closes: #830503)
  * Add patch to build with either openssl 1.0.2 or 1.1.0 (Closes: #828466)
    - Thanks to Sebastian Andrzej Siewior for the patch
  * Generate opendkim.service in postinst instead of shipping it in the
    package (Closes: #837374)

 -- Scott Kitterman <scott@kitterman.com>  Mon, 24 Oct 2016 14:15:53 -0400

opendkim (2.11.0~alpha-6) experimental; urgency=medium

  * Install opendkim.service.generate in its own directory

 -- Scott Kitterman <scott@kitterman.com>  Mon, 04 Jul 2016 23:02:57 -0400

opendkim (2.11.0~alpha-5) experimental; urgency=medium

  * Since the variables that can be set from the environment directly a
    systemd unit file, generate opendkim.service based on /etc/default/
    opendkim
  * Update /etc/default to include a few additional options as a result

 -- Scott Kitterman <scott@kitterman.com>  Mon, 04 Jul 2016 16:54:02 -0400

opendkim (2.11.0~alpha-4) experimental; urgency=medium

  * Reorder startup options to match the requirements listed in the help file
    - Completes fix for (Closes: #792458)
  * Stop installing historical dkim-milter changelog (RELEASE-NOTES.Sendmail)
  * Add .m4 file to ease sendmail integration and related README.Sendmail
    (Closes: #804806)
  * Make opendkim user and group variables in both the service and sysv init
    files and then add the definition to /etc/default/opendkim to make it
    easier to change which user and group opendkim runs under (Closes:
    #778871)
  * Add opendkim run directory to /etc/default/opendkim to make it easier to
    change it to support, for example, use of a Unix socket for a chrooted
    postfix (Closes: #797492)

 -- Scott Kitterman <scott@kitterman.com>  Sat, 02 Jul 2016 20:18:13 -0400

opendkim (2.11.0~alpha-3) experimental; urgency=medium

  * Hard-code shell in debian/rules for reproducibility
  * Try again on configure for kfreebsd* and hurd
    - Fix debian/rules
    - Add debian/opendkim-tools.install/manpages.hurd-i386 to account for
      opendkim-spam not being built when libopendbx is not available

 -- Scott Kitterman <scott@kitterman.com>  Thu, 30 Jun 2016 23:01:45 -0400

opendkim (2.11.0~alpha-2) experimental; urgency=medium

  * Update debian/copyright to reflect change of copyright holder to The
    Trusted Domain Project
  * Update experimental to match changes in unstable (2.10.3-4)
    - Tighten opendkim depend version on libopendkim11 to prevent openssl
      version mismatch problems (Closes: #821898)
    - Change build-dep for libunbound-dev to linux-any since it is now linux
      only
    - Bump standards version to 3.9.8 without further change
  * Change build-dep for libopendbx1-dev to linux-any, kfreebsd-any since it
    is not available on hurd-i386
  * Adjust debian/rules to use correct configure options on kfreebsd and hurd
    due to not all build-depends being available
  * Drop obsolete build-depends on libxml2-dev and related configure option

 -- Scott Kitterman <scott@kitterman.com>  Tue, 17 May 2016 20:03:01 -0400

opendkim (2.11.0~alpha-1) experimental; urgency=medium

  * New upstream alpha release
    - Rename libopendkim10 to libopendkim11 to match new soname
    - Update symobls file
    - Rename debian/libopendkim10.*
  * Add --enable-poll to use poll() instead of select() for asynchronous I/O
    as discussed on opendkim-users@lists.opendkim.org
  * Add --enable-sender_macro to simplify identifing signing domain

 -- Scott Kitterman <scott@kitterman.com>  Tue, 19 Jan 2016 00:53:23 -0500

opendkim (2.10.3-3) unstable; urgency=medium

  * Correct install location of systemd service file
  * Drop debian/patches for upstream service file and use custom service file
    in debian/
  * Adjust /etc/default/opendkim to work with either systemd or sysv init
  * Adjust debian/openddkim.init to work with modified default file

 -- Scott Kitterman <scott@kitterman.com>  Tue, 07 Jul 2015 01:15:38 -0400

opendkim (2.10.3-2) unstable; urgency=medium

  * Ship systemd service file along with sysv init (LP: #1452538)
  * Fix upstream service file to recreate /var/run/opendkim on boot and to use
    appropriate paths for Debian
    - Thanks to Ben Thielsen for the patch

 -- Scott Kitterman <scott@kitterman.com>  Sun, 05 Jul 2015 14:50:56 -0400

opendkim (2.10.3-1) unstable; urgency=medium

  * Add contrib/docs/chroot chrooting README to opedkim.docs
  * Add dns-root-data to opendkim Depends for DNSSEC support and adjust
    debian/opendkim.conf to for the correct path to the trust anchor
    (Closes: #751560)
  * Agreed Maintainer/Uploader swap

 -- Scott Kitterman <scott@kitterman.com>  Mon, 29 Jun 2015 23:56:53 -0400

opendkim (2.10.1-2) unstable; urgency=medium

  * Upload to unstable
  * Use daemon name vice filename in init when checking to see if the pid file
    is stale or not (Closes: #783260)
  * Add /etc/dkimkeys directory with appropriate permissions set in postinst
    for securely storing DKIM private keys (Closes: #706061)
  * Update and improve key managemnet discussion in README.Debian
  * Add unbound to opendkim suggests and UnboundConfigFile so shipped config
    file to ease setup of DNSSEC (Closes: #751560)
  * Use exit instead of return in opendkim.init in start, stop, status
    (Closes: #776590)
  * Update dates in debian/copyright
  * Update comments in debian/opendkim.conf
  * Fix missing text in libopendkim-dev long description (Closes:
    #780140)
  * Fix dubplicated text in libvbr-dev long description (Closes:
    #780619)
  * Add --with-domain=localhost to configure to make the build
    reproducible (Closes: #782459)
    - Thanks to Reiner Herrmann for the report and the patch
  * Delete opendkim.postrm - we don't want to delete the user on purge
    since it may still own key files
  * Use pathfind from devref 6.4 to remove hard coded paths to
    restorecon and fix lintian command-with-path-in-maintainer-script
    warning

 -- Scott Kitterman <scott@kitterman.com>  Sun, 26 Apr 2015 15:59:57 -0400

opendkim (2.10.1-1) experimental; urgency=medium

  * New upstream release
    - Bump libopendkim to libopendkim10 due to upstream soname change: Update
      debian/control and rename libopendkim10.docs, libopendkim10.install, and
      libopendkim10.symbols
    - Update libopendkim symbols file to remove symbols for dropped ADSP
      functionality (Obsolete)
    - Drop all patches, previously cherrypicked from upstream
    - ADSP support removed.  Remove references to opendkim-testadsp from
      debian/control, opendkim-tools.install, and opendkim-tools.manpages
  * Bump standards version to 3.9.6 without further change

 -- Scott Kitterman <scott@kitterman.com>  Tue, 24 Feb 2015 00:24:58 -0500

opendkim (2.9.2-2) unstable; urgency=medium

  * Cherry-pick bug fixes from upstream development of version 2.10
    - 0000-Replace-overlapping-strlcpy-with-memmove-in-dkim_get.patch
    - 0000-Treat-CR-as-end-of-line.patch
    - 0001-Fix-bug-177-Plug-leaking-result-structures-when-Open.patch
    - 0002-LIBOPENDKIM-Tighten-relaxed-modes-to-break-on-only-D.patch
    - 0003-Feature-request-178-Add-F-flag-to-opendkim-genzone-s.patch
    - 0004-Be-careful-about-what-spaces-we-toss-in-header-field.patch
    - 0005-Make-dkim_header-more-strict-about-the-inputs-it-wil.patch
    - 0006-LIBOPENDKIM-If-a-signature-fails-to-verify-for-eithe.patch
    - 0009-Handle-parameters-safely-in-repute.php.-Reported-by-.patch
    - 0010-Handle-empty-values-of-i-reported-by-MTAs.patch
    - 0011-Make-a-reference-from-the-DNS-service-functions-to-d.patch
    - 0013-Patch-32-Re-arrange-the-execution-logic-to-drop-priv.patch

 -- Scott Kitterman <scott@kitterman.com>  Fri, 28 Nov 2014 16:20:24 -0500

opendkim (2.9.2-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 19 Apr 2014 12:28:13 -0400

opendkim (2.9.1-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Mon, 17 Mar 2014 19:42:23 -0400

opendkim (2.9.1~Beta4-1) experimental; urgency=low

  * New upstream beta release

 -- Scott Kitterman <scott@kitterman.com>  Wed, 12 Mar 2014 17:48:45 -0400

opendkim (2.9.0-3) unstable; urgency=medium

  * Disable test suite due to bogus errors even harder (Closes: #740486)
    - Forgot to disable mips/mipsel, Thanks to Dejan Latinovic for the patch

 -- Scott Kitterman <scott@kitterman.com>  Wed, 05 Mar 2014 16:25:34 -0500

opendkim (2.9.0-2) unstable; urgency=low

  * Disable test suite on all archs due to undiagnosed race condition in the
    test suite that causes multiple FTBFS, but doesn't appear reflect any
    actual program problems

 -- Scott Kitterman <scott@kitterman.com>  Sat, 08 Feb 2014 23:22:17 -0500

opendkim (2.9.0-1) unstable; urgency=low

  * New upstream release
    - Update symbols
  * Lengthen time between opendkim tests to increase test reliability
  * Bump standards version to 3.9.5 without further change

 -- Scott Kitterman <scott@kitterman.com>  Fri, 17 Jan 2014 23:27:51 -0500

opendkim (2.8.4-2) unstable; urgency=low

  * Skip tests on kfreebsd-* as results are incredibly inconsistent due to
    system issues and not package bugs

 -- Scott Kitterman <scott@kitterman.com>  Wed, 17 Jul 2013 15:51:17 -0400

opendkim (2.8.4-1) unstable; urgency=low

  * New upstream release
  * Slow down tests on mips and mipsel to give them more time to pass
  * Update debian/watch from update provided from the PTS

 -- Scott Kitterman <scott@kitterman.com>  Tue, 16 Jul 2013 16:53:03 -0400

opendkim (2.8.4~beta2-1) experimental; urgency=low

  * New upstream beta release (Closes: #713968)

 -- Scott Kitterman <scott@kitterman.com>  Sun, 07 Jul 2013 03:45:06 -0400

opendkim (2.8.4~beta1-1) experimental; urgency=low

  * New upstream release
  * Revert inadvertent direct dependency on db5.3 included in last upload

 -- Scott Kitterman <scott@kitterman.com>  Mon, 10 Jun 2013 13:52:17 -0400

opendkim (2.8.4~beta0-1) experimental; urgency=low

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Fri, 24 May 2013 15:11:36 -0400

opendkim (2.8.3-1) unstable; urgency=low

  * New upstream release
  * Added doc-base registration

 -- Scott Kitterman <scott@kitterman.com>  Mon, 06 May 2013 15:47:07 -0400

opendkim (2.8.2-1) unstable; urgency=low

  * Upload to unstable
  * New upstream release
    - Drop debian/patches/Wformat_fixes.patch, originally backported from
      upstream and included in this release
  * Replace commented out entry for ADSPDiscard with an equally commented out
    entry for ADSPAction to match upstream changes (Closes: #701987)

 -- Scott Kitterman <scott@kitterman.com>  Fri, 29 Mar 2013 15:38:21 -0400

opendkim (2.8.1-1) experimental; urgency=low

  * New uptream release
  * Switch to source/format 3.0 (quilt)
  * Add patch from upstream to fix -Wformat warnings

 -- Scott Kitterman <scott@kitterman.com>  Tue, 19 Mar 2013 21:50:10 -0400

opendkim (2.8.0-1) experimental; urgency=low

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Mon, 25 Feb 2013 17:06:21 -0500

opendkim (2.8.0~beta6-1) experimental; urgency=low

  * New upstream beta release
    - Drop debian/patches/warnings-fix.patch and added-linking.patch both
      included from upstream
  * Remove no longer needed quilt patch system

 -- Scott Kitterman <scott@kitterman.com>  Wed, 20 Feb 2013 15:47:45 -0500

opendkim (2.8.0~beta5-1) experimental; urgency=low

  * New upstream beta release
  * Add debian/patches/warnings-fix.patch from upstream to correct issues that
    led to compiler warnings
  * Add debian/patches/added-linking.patch to add missing libs for correct
    linking
  * Add quilt to build-depends, add README.source, and add with quilt to
    debian/rules

 -- Scott Kitterman <scott@kitterman.com>  Sat, 16 Feb 2013 00:02:12 -0500

opendkim (2.8.0~beta4-1) experimental; urgency=low

  * New upstream beta release

 -- Scott Kitterman <scott@kitterman.com>  Tue, 12 Feb 2013 17:30:09 -0500

opendkim (2.8.0~beta3-1) experimental; urgency=low

  * New upstream beta relaese

 -- Scott Kitterman <scott@kitterman.com>  Mon, 11 Feb 2013 01:38:27 -0500

opendkim (2.8.0~beta2-2) experimental; urgency=low

  * Use MALLOC_CHECK_=0 for tests on kfreebsd-i386 due to issues with the
    squeeze kernel used on the buildds
    - To be dropped after wheezy release and buildds are updated

 -- Scott Kitterman <scott@kitterman.com>  Fri, 08 Feb 2013 15:38:22 -0500

opendkim (2.8.0~beta2-1) experimental; urgency=low

  * New upstream beta release
    - Drop local miltertest/miltertest.c and and opendkim/opendkim.c changes
      that had been backported from upstream

 -- Scott Kitterman <scott@kitterman.com>  Wed, 06 Feb 2013 21:41:19 -0500

opendkim (2.8.0~beta1-1) experimental; urgency=low

  * New upstream beta release
    - Drop local opendkim/tests changes in favor of overriding dh_auto_test on
      mips to use the new upstream method for increasing connect retry interval
      (MILTERTEST_RETRY_SPEED_FACTOR)
  * Backport fix from upstream to make MILTERTEST_RETRY_SPEED_FACTOR work
  * Backport fix from upstream to remove left over attempt to free variable
    that is now static (opendkim/opendkim.c)

 -- Scott Kitterman <scott@kitterman.com>  Fri, 01 Feb 2013 00:11:24 -0500

opendkim (2.8.0~beta0-3) experimental; urgency=low

  * Extend mt.set_timeout(300) to all opendkim tests and also increase number
    of retries and retry interval to use the extra time
  * Fix typo in closes for last changelog entry

 -- Scott Kitterman <scott@kitterman.com>  Wed, 30 Jan 2013 23:17:32 -0500

opendkim (2.8.0~beta0-2) experimental; urgency=low

  * Add mt.set_timeout(300) to opendkim/tests/t-sign-ss.lua to try to fix mips
    test failure due to opendkim not being ready yet.
  * Add test for existence of /var/run/opendkim to opendkim.postinst
    (Closes: #695610)

 -- Scott Kitterman <scott@kitterman.com>  Wed, 30 Jan 2013 01:05:31 -0500

opendkim (2.8.0~beta0-1) experimental; urgency=low

  * New upstream beta release
    - Drop local configure, miltertest, and test suite changes as they are
      all incorporated upsteram
    - Rename libopendkim8 to libopendkim9 to match upstream soname bump and
      update debian/control and debian/libopendkim8.*
    - Update libopendkim symbols file
  * Drop obsolescent dkim-reputation flag from configure in debian/rules
    - No longer particularly useful and leads to DNS queries even when network
      tests are disabled
    - Remove libdkimrep1/libdkimrep-dev from debian/control as well as related
      debian/* files
  * Drop opendkim-reputation binary as well as debian/* files as the binary
    makes little sense without libdkimrep

 -- Scott Kitterman <scott@kitterman.com>  Sun, 27 Jan 2013 00:00:07 -0500

opendkim (2.7.4-5) experimental; urgency=low

  [ Scott Kitterman ]
  * Patch configure.ac and the source to allow bsd/string.h and -lbsd
    as an alternative and remove -lbsd from LDFLAGS in debian/rules.

  [ Adam Conrad ]
  * Pass --disable-live-testing to configure to skip tests that need
    internet access, as those fail on the restrictive buildd network.
  * Guard four more testsuite tests with LIVE_TESTS check, as they
    generate DNS traffic that gets shut down by restrictive networks.
  * Build-depend on and use dh-autoreconf, and drop the manual copy
    of config.{sub,guess} in debian/rules now that it's automagic.
  * Call dh(1) with --parallel to marginally improve build times.
  * Simplify cross-build handling to always pass --host and --build.

 -- Scott Kitterman <scott@kitterman.com>  Sat, 19 Jan 2013 13:02:34 -0500

opendkim (2.7.4-4) experimental; urgency=low

  * Added needed configure changes for TCP sockets in the test suite

 -- Scott Kitterman <scott@kitterman.com>  Mon, 14 Jan 2013 23:21:09 -0500

opendkim (2.7.4-3) experimental; urgency=low

  * Include forgotten changes to miltertest from the last upload

 -- Scott Kitterman <scott@kitterman.com>  Mon, 14 Jan 2013 21:47:13 -0500

opendkim (2.7.4-2) experimental; urgency=low

  * Backport pre-release patch from 2.8.0 to add the option to build to use a
    TCP socket instead of a Unix socket in the test suite in order to work
    around auto* induced path length issues in socket names
  * Add with-test-socket=inet to configure options in debian/rules

 -- Scott Kitterman <scott@kitterman.com>  Mon, 14 Jan 2013 19:12:30 -0500

opendkim (2.7.4-1) experimental; urgency=low

  * New upstream release
    - Fixed selector logging error (Closes: #695145)
  * Drop reprrd from configuration since it pulls in an insane dependency
    stack at the moment

 -- Scott Kitterman <scott@kitterman.com>  Tue, 08 Jan 2013 03:00:16 -0500

opendkim (2.7.3-1) experimental; urgency=low

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 08 Dec 2012 14:38:19 -0500

opendkim (2.7.2-1) experimental; urgency=low

  * New upstream release
    - Change libopendkim7 to libdkim8 to match new soversion (update .install/
      .docs files and libopendkim-dev depends)
    - Ship .pc files for libopendkim-dev and libvbr-dev
    - Add pkg-config to build-depends
    - Add libbsd-dev to build-depends instead of using embedded copies of
      strlcpy/strlcat and add -lbsd to LDFLAGS for proper linking
    - Drop enable-oversign configure option, enabled by default now
    - Add New configure options for experimental DKIM based reputation
      options, enable-dkim_reputation and enable-reprrd (and add rrdtool and
      librrd-dev to build-depends)
    - Add libdkimrep1 and libdkimrep-dev to debian/control along with related
      install, symbpols, docs, and manpages files
    - Add librbl1 and librbl-dev to debian/control along with related install
      and symbols files
    - Add opendkim-reprrd, libreprrd1 and libreprrd-dev to debian/control
      along with related install and symbols files
    - Build with-libxml2 and with-jansson to provide reputation data exchange
      formats (and add libjansson-dev and libxml2-dev to build-depends)
    - Build with enable-query_cache so the opendkim library will keep a local
      cache
    - Build with-libmemcached and add libmemcached-dev to build-depends to
      enable the memcached data set type
  * Reorganize debian/rules and install/dirs/manpages/docs/examples files for
    installation from debian/tmp and dehlper 7 style rules with overrides
  * Bump compat and debhelper version requirement to 9
  * Add symbols files for existing libraries
  * Now that OpenDBX is available in Debian, build with-odbx and
    with-sql-backend (MySQL) (and add libopendbx1-dev to build-depends)
    (Closes: #656767)
  * Build with-sasl to support SASL authentication for LDAP (and add
    libsasl2-dev to build-depends)
  * Bump standards version to 3.9.4 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 19 Nov 2012 20:09:05 -0500

opendkim (2.6.8-2) unstable; urgency=low

  * No-change upload to unstable

 -- Scott Kitterman <scott@kitterman.com>  Tue, 30 Oct 2012 12:51:42 +0000

opendkim (2.6.8-1) experimental; urgency=low

  * New upstream security release to add capability to exclude use of
    insecure keys (Closes: #691394, LP: #1071139)

 -- Scott Kitterman <scott@kitterman.com>  Thu, 25 Oct 2012 01:04:27 -0400

opendkim (2.6.7-1) experimental; urgency=low

  * New upstream release
    - Drop obsolete configure option enable-selector_header

 -- Scott Kitterman <scott@kitterman.com>  Mon, 23 Jul 2012 18:17:11 -0400

opendkim (2.6.6-1) experimental; urgency=low

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Mon, 23 Jul 2012 08:28:16 -0400

opendkim (2.6.4-1) experimental; urgency=low

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sun, 15 Jul 2012 00:04:24 -0400

opendkim (2.6.2-1) unstable; urgency=low

  * New upstream release (addresses three bugs that affect the Debian package)
    - Fix bug #SF3539449: Clarify legal "Socket" values.  Requested by Scott
      Kitterman.
    - Fix bug #SF3539493: Handle certain cases of data set names that appear
      to be comma-separated lists which include IPv6 addresses.  Reported by
      Scott Kitterman. (Closes: #679548)
  * Use restorecon to apply a SE Linux label after creating a run dir
    (Closes: #679852)

 -- Scott Kitterman <scott@kitterman.com>  Mon, 02 Jul 2012 14:44:38 -0400

opendkim (2.6.1-1) unstable; urgency=low

  * New upstream release
  * Use CFLAGS, CPPFLAGS, and LDFLAGS from dpkg-buildflags

 -- Scott Kitterman <scott@kitterman.com>  Sun, 24 Jun 2012 23:58:40 -0400

opendkim (2.6.0-1) unstable; urgency=low

  * New upstream release (now DFSG free so tarball repacking no longer
    required)
    - Rename libopendkim6 to libopendkim7 to match new soname
      - Update package and dependencies in debian/control
      - Rename .install and .doc files
    - Drop --enable-xtags from configure in debian/rules since it is now on by
      default
    - Update debian/copyright
    - Remove dversionmangle from debian/watch
    - Update README.Debian to reflect documentation no longer being stripped
  * Correct copyright/license information in libar/ar-strl.h based on feedback
    from upstream (correction already implemented upstream for the next
    relese)
  * Set --sysconfdir=/etc in configure so that generated man pages refer to
    the correct configuration file locations

 -- Scott Kitterman <scott@kitterman.com>  Fri, 08 Jun 2012 14:38:25 -0400

opendkim (2.5.2+dfsg-3) unstable; urgency=low

  * Re-add missing --with-db flag for configure

 -- Scott Kitterman <scott@kitterman.com>  Fri, 13 Apr 2012 10:41:37 -0400

opendkim (2.5.2+dfsg-2) unstable; urgency=low

  * Add patch from upstream to fix stray debug code left in opendkim-genstats
  * Split opendkim into opendkim and opendkim-tools since the command line
    support tools are now bigger than the application
  * Add status option to /etc/init.d/opendkim
    - Add depends on lsb-base
  * Add Description to /etc/init.d/opendkim header
  * Enable Vouch By Reference support:
    - Add --enable-vbr in debian/rules
    - Update libopendkim install files to be more specific and not install
      libvbr related files
    - Add libvbr2 and libvbr-dev to debian/control
    - Add debian/libvbr2.docs, libvbr2.install, and libvbr-dev.install
  * Enable extensions for adding arbitrary experimental signature tags and
    values in libopendkim (neeeded for ATPS support)
    - Add --enable-xtags in debian/rules
  * Enable support for RFC 6541 DKIM Authorized Third-Party Signatures (ATPS)
    - Add --enable-atps in debian/rules
  * Enable support for optional oversigning of header fields to prevent
    malicious parties from adding additional instances of the field
    - Add --enable-oversign to debian/rules
    - Modify debian/opendkim.conf to use OversignHeaders for From by default
  * Add required build-arch and build-indep targets to debian/rules
  * Fixed formating and versioning of existing opendkim.NEWS entry
  * Added new opendkim.NEWS entry to describe changed defaults with this
    revision
  * Update debian/copyright (Closes: #664132)
  * Add debian/watch
  * Remove unneeded shlibs:Depends for libdkim-dev

 -- Scott Kitterman <scott@kitterman.com>  Tue, 10 Apr 2012 23:58:52 -0400

opendkim (2.5.2+dfsg-1) unstable; urgency=low

  * New upstream release
    - Repacked tarball to remove non-free IETF drafts and RFCs
    - Updated docs/Makefile.in/am, and README to remove references removed
      non-free documentation
  * Add Homepage: to debian/control

 -- Scott Kitterman <scott@kitterman.com>  Mon, 09 Apr 2012 20:03:48 -0400

opendkim (2.5.0.1+dfsg-1) unstable; urgency=low

  * Add myself to uploaders
  * Merge package updates back from Ubuntu
  * New upstream release
    - Repacked tarball to remove non-free IETF drafts and RFCs
    - Updated docs/Makefile.in/am, and README to remove references removed
      non-free documentation
    - Add opendkim.NEWS to include note about incompatible change between
      BodyLengths setting and BodyLengthDB
    - Drop no longer recognized configure options, --enable-ztags,
      --enable-dnsupgrade, --enable-report_intervals, and
      --enable-bodylength_db
    - Drop local changes to libar/Makefile.in/am, incorporated upstream
    - Remove debian/rules to rename opendkim-genkey and opendkim-importstats,
      incorporated upstream
    - Install opendkim-convert-keylist in /usr/bin instead of /usr/sbin with
      other scripts
  * Update libopendkim to libopendkim6 for new so version
    - Change package name in debian/control
    - Update libopendkim-dev depends
    - Rename .install and .docs files
    - Update dh_shlibdeps call in debian/rules
  * Bump standards version to 3.9.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Wed, 14 Mar 2012 22:34:48 -0400

opendkim (2.1.3+dfsg-1) unstable; urgency=low

  * New upstream version.
  * opendkim is now under the 3-clause BSD license, although the Sendmail-
    owned components remain under the Sendmail license. The copyright file
    has been updated to reflect this change.
  * Debian-specific patch to include libresolv in libar to work around -z defs
    as a linker flag. This library isn't meant for consumption by other source
    packages, but using -z defs does cause a build failure, and I'd rather
    keep the error in than risk missing a dependency because it's gone.
  * libopendkim's major version number has changed; rename to libopendkim2

 -- Mike Markley <mike@markley.org>  Fri,  6 Aug 2010 10:58:54 -0700

opendkim (2.0.1+dfsg-1) unstable; urgency=low

   * New upstream version.
   * Put config.sub/guess in build-aux/ instead of top-level. Closes: #575296
   * Include dir in opendkim.pc has been corrected to what will ship from
     upstream in future releases (<includedir>/opendkim).

 -- Mike Markley <mike@markley.org>  Wed, 31 Mar 2010 12:55:26 -0700

opendkim (2.0.0+dfsg-1) unstable; urgency=low

  * New upstream version.
  * The libopendkim ABI has changed, resulting in an soname change.
    - libopendkim0 package renamed to libopendkim1.
  * New LDAP and Lua support have been enabled.
  * Wrote clearer package descriptions. Closes: #572725
  * Added build-dependency on autotools-dev. For now, the package ships with
    very old config.sub and config.guess files, but this will be resolved
    upstream in the near future.
    - Also removed extraneous top-level config.{sub,guess} files.
  * Applied two upstream patches for off-by-one errors, including one that can
    cause crashes when signing.
  * debian/copyright file was not accurate; it has been updated to reflect the
    fork from the dkim-milter project. The dkim-milter license should be fully
    compatible with the BSD license of this fork.

 -- Mike Markley <mike@markley.org>  Wed, 17 Mar 2010 13:04:28 -0700

opendkim (1.1.0+dfsg-1) unstable; urgency=low

  * New upstream version.
  * Look for __res_mkquery() instead of res_mkquery in configure to solve build
    issues on multiple platforms. Closes: #544802
  * Removed --enable-commaize (it has been removed upstream as
    unnecessary)

 -- Mike Markley <mike@markley.org>  Sat, 19 Sep 2009 16:25:10 -0700

opendkim (1.0.0+dfsg-1) unstable; urgency=low

  * Initial upload. Closes: #543426, #542948
  * Patched configure to support libunbound correctly.
  * Patched libopendkim/Makefile.am and Makefile.in to support libunbound &
    libar correctly.

 -- Mike Markley <mike@markley.org>  Mon, 24 Aug 2009 18:47:41 -0700
